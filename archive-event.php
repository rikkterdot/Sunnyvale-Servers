<?php
get_header();
?>

    <div id="page-title" class="parallax">
        <div class="container">
            <h1>Upcoming Events</h1>
        </div>
    </div>

    
    <div class="texture-bg">
        <div style="height: 2px;"></div>
        <div class="yellow-bar no-mrg-top"></div>
        <div id="single-wrap" class="container">
            <?php 
                $posts = get_posts(array(
                    'post_type'         => 'event',
                    'posts_per_page'    => -1,
                    'meta_key'          => 'date_picker',
                    'orderby'           => 'meta_value',
                    'order'             => 'DESC'
                ));
            ?>
            <?php if( $posts ): ?>
                <div id="events_archive_wrap">
                    <?php $delay = 2; foreach( $posts as $post ): setup_postdata( $post ); ?>
                        <div class="event_item  wow fadeInDown delay0-<?php echo $delay; ?>s">
                            <h2><span><?php the_field('when'); ?></span> - <?php the_title(); ?></h2>
                            <div class="excerpt"><?php the_excerpt(); ?></div>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="angled-btn wow fadeInDown delay0-<?php echo $delay; ?>s">View Event</a>

                    <?php $delay++; endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            <?php else: ?>
                <h4>There are currently no events.</h4>
            <?php endif; ?>
        </div>
    </div>

<?php get_footer(); ?>
