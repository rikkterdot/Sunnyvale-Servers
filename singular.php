<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

    <?php get_template_part( 'templates/partials/page', 'title' ); ?>
    
    <div class="texture-bg">
        <div style="height: 2px;"></div>
        <div class="yellow-bar no-mrg-top"></div>
        <div id="single-wrap" class="container">
            <?php the_content(); ?>
        </div>
    </div>

<?php get_footer(); ?>
