var gulp         = require('gulp'),
	sass         = require('gulp-sass'),
	browserSync  = require('browser-sync'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify       = require('gulp-uglify-es').default,
	jshint       = require('gulp-jshint'),
	rename       = require('gulp-rename'),
	cssnano      = require('cssnano'),
	postcss      = require('gulp-postcss'),
	concat       = require('gulp-concat'),
	order        = require("gulp-order"),
	sourcemaps   = require('gulp-sourcemaps'),
	addsrc       = require('gulp-add-src'),
	config       = require('./.config');

gulp.task('css', function (done) {

	var stream = gulp.src('src/scss/style.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({errLogToConsole: true}))
		.pipe(autoprefixer('last 4 version'));
	if ( config.css.prepend && config.css.prepend.length > 0 ) {
		stream = stream.pipe(addsrc.prepend(config.css.prepend));
	}
	if ( config.css.append && config.css.append.length > 0 ) {
		stream = stream.pipe(addsrc.append(config.css.append));
	}
	stream = stream.pipe(concat('style.css'))
		.pipe(gulp.dest(config.css.destination))
		.pipe(postcss([cssnano()]))
		.pipe(rename({ suffix: '.min' }))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(config.css.destination))
		.pipe(browserSync.reload({stream:true}));

		//browserSync.reload();
		
	done();

});

gulp.task('js', function (done) {
	
	if ( config.js.footer.use ) {
		var stream = gulp.src('src/js/footer/**/*.js')
			.pipe(sourcemaps.init())
			.pipe(jshint('.jshintrc'))
			.pipe(jshint.reporter('default'));
		if ( config.js.footer.order && config.js.footer.order.length > 0 ) {
			stream = stream.pipe(order(config.js.footer.order));
		}
		if ( config.js.footer.prepend && config.js.footer.prepend.length > 0 ) {
			stream = stream.pipe(addsrc.prepend(config.js.footer.prepend));
		}
		if ( config.js.footer.append && config.js.footer.append.length > 0 ) {
			stream = stream.pipe(addsrc.append(config.js.footer.append));
		}
		stream = stream.pipe(concat('footer.js'))
			.pipe(gulp.dest(config.js.destination));
		if ( config.js.footer.minify ) {
			stream = stream.pipe(uglify())
				.pipe(rename({ suffix: '.min' }));
		}
		stream = stream.pipe(sourcemaps.write('./maps'));
		stream = stream.pipe(gulp.dest(config.js.destination));
		stream = stream.pipe(browserSync.reload({stream:true, once: true}));
	}
	
	if ( config.js.header.use ) {
		var stream = gulp.src('src/js/header/**/*.js')
			.pipe(sourcemaps.init())
			.pipe(jshint('.jshintrc'))
			.pipe(jshint.reporter('default'));
		if ( config.js.header.order && config.js.header.order.length > 0 ) {
			stream = stream.pipe(order(config.js.header.order));
		}
		if ( config.js.header.prepend && config.js.header.prepend.length > 0 ) {
			stream = stream.pipe(addsrc.prepend(config.js.header.prepend));
		}
		if ( config.js.header.append && config.js.header.append.length > 0 ) {
			stream = stream.pipe(addsrc.append(config.js.header.append));
		}
		stream = stream.pipe(concat('header.js'))
			.pipe(gulp.dest(config.js.destination));
		if ( config.js.header.minify ) {
			stream = stream.pipe(uglify())
				.pipe(rename({ suffix: '.min' }));
		}
		stream = stream.pipe(sourcemaps.write('./maps'));
		stream = stream.pipe(gulp.dest(config.js.destination));
		stream = stream.pipe(browserSync.reload({stream:true, once: true}));
	}
	
	done();
	
});

gulp.task('browser-sync', function(done) {
	browserSync.init({
		proxy: config.local_url,
	});
	done();
});

gulp.task('bs-reload', function (done) {
	browserSync.reload();
	done();
});

gulp.task('default', gulp.series('css', 'js', 'browser-sync', function (done) {
	gulp.watch("src/scss/*.scss", gulp.parallel('css'), gulp.parallel('bs-reload'));
	gulp.watch("src/scss/**/*.scss", gulp.parallel('css'));
	gulp.watch("src/scss/**/**/*.scss", gulp.parallel('css'));
	gulp.watch("src/js/*.js", gulp.parallel('js'));
	gulp.watch("src/js/**/*.js", gulp.parallel('js'));
	gulp.watch("./*.php", gulp.parallel('bs-reload'));
	gulp.watch("./**/*.php", gulp.parallel('bs-reload'));
	done();
}));
