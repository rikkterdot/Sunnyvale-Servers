jQuery(document).ready(function($){

	new WOW().init();

	$(window).load(function(){
		checkTop();
	});
	$(window).scroll(function(){
		checkTop();
	});

	$('.parallax').parallax({speed : 0.20, grow:false, gOffset: "0", coffset: "0"});

	$("header#masthead").hover(function(){
		$("#site_wrap").addClass("dim");
	}, function(){
		$("#site_wrap").removeClass("dim");
	});

	function checkTop(){
		if(!$(window).scrollTop()) {
		  $("header#masthead").removeClass("notAtTop");
		  $("#event_wrap").removeClass("notAtTop");
		} else {
		  $("header#masthead").addClass("notAtTop");
		  $("#event_wrap").addClass("notAtTop");
		};

		if($(window).scrollTop() < 700) {
		  $("#event_wrap").removeClass("hide-events");
		} else {
		  $("#event_wrap").addClass("hide-events");
		};

	};

	$.fn.pvisiblescroll = function() {
		var element = $(this);
		var elementTop = element.offset().top;
		var viewportTop = $( window ).scrollTop();
		var top = ( elementTop - viewportTop );
		var elementBottom = element.offset().top + element.height();
		var viewportBottom = viewportTop + $( window ).height();
		var bottom = ( elementBottom - viewportBottom );
		var percent = top / ( top - bottom );
		return percent;
	};

	$.fn.pvisible = function() {
		var eTop = this.offset().top;
		var eBottom = eTop + this.height();
		var wTop = $(window).scrollTop();
		var wBottom = wTop + $(window).height();
		var totalH = Math.max(eBottom, wBottom) - Math.min(eTop, wTop);
		var wComp = totalH - $(window).height();
		var eIn = this.height() - wComp;
		return (eIn <= 0 ? 0 : eIn / this.height() * 100);
	};

	$.fn.isInViewport = function( toffset ) {
		if ( ! toffset ) {
			toffset = 0;
		}
		var elementTops = $(this);
		if (elementTops.length) {
			var elementTop = elementTops.offset().top;
			var elementBottom = elementTop + $(this).outerHeight();
			var viewportTop = $(window).scrollTop() + toffset;
			var viewportBottom = viewportTop + $(window).height();
			return elementBottom > viewportTop && elementTop < viewportBottom;
		}
	};


	// anchor tag scroll mechanism
	if ( document.querySelectorAll ) {

		var anchors = document.querySelectorAll( 'a[href^="#"]' );
		var func = function (e) {
			e.preventDefault();
			var selector = document.querySelector( this.getAttribute( 'href' ) );
			if ( selector ) {
				selector.scrollIntoView( {
					behavior: 'smooth'
				} );
			}
		};
		for ( var i = 0; i < anchors.length; i++ ) {
			var anchor = anchors[i];
			anchor.addEventListener( 'click', func );
		}

	}



});