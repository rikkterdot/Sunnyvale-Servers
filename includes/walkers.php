<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Special_Nav_Walker extends Walker_Nav_Menu {
	private $depth_index = 0;
	private $breakpoint = null;
	private $enabled = false;
	private $subtitle_secondary = null;
	public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {
		if ( $depth == 1 ) {
			if ( $this->breakpoint != null ) {
				if ( $this->breakpoint == $this->depth_index ) {
				$output .= 			'</ul>';
				$output .= 		'</div>';
				$output .= 		'<div class="menu-wrap secondary">';
				$output .= 			'<div class="title">' . $this->subtitle_secondary . '</div>';
				$output .= 			'<ul class="sub-menu">';
				}
			}
		}
		parent::start_el( $output, $item, $depth, $args, $id );
		$orion_mega_enabled              = get_field('orion_mega_enabled', $item);
		$orion_menu_blurb                = get_field('orion_menu_blurb', $item);
		$orion_menu_enable_secondary     = get_field('orion_menu_enable_secondary', $item);
		$orion_menu_secondary_title      = get_field('orion_menu_secondary_title', $item);
		$orion_menu_secondary_breakpoint = get_field('orion_menu_secondary_breakpoint', $item);
		$orion_promo_title               = get_field('orion_promo_title', $item);
		$orion_promo_blurb               = get_field('orion_promo_blurb', $item);
		$orion_promo_link                = get_field('orion_promo_link', $item);
		$orion_promo_background          = get_field('orion_promo_background', $item);
		if ( $depth == 0 ) {
			$this->enabled = $orion_mega_enabled;
			$this->depth_index = 0;
			if ( $orion_mega_enabled ) {
				$this->breakpoint = $orion_menu_enable_secondary ? $orion_menu_secondary_breakpoint : null;
				$this->subtitle_secondary = $orion_menu_secondary_title;
				$output = str_replace( 'class="', 'class="mega-enabled ', $output );
				$output .= '<div class="mega ' . ( $orion_menu_enable_secondary ? 'has-secondary' : '' ) . '">';
				$output .= 	'<div class="container">';
				$output .= 		'<div class="promo">';
				$output .= 			'<div class="bg"></div>';
				$output .= 			'<img src="' . $orion_promo_background . '" class="relief" />';
				$output .= 			'<div class="context">';
				$output .= 				'<div class="title">' . $orion_promo_title . '</div>';
				$output .= 				'<div class="blurb">' . $orion_promo_blurb . '</div>';
				$output .= 				'<a class="cta chevron-text" href="' . $orion_promo_link['url'] . '" target="' . $orion_promo_link['target'] . '">' . $orion_promo_link['title'] . '</a>';
				$output .= 			'</div>';
				$output .= 		'</div>';
				$output .= 		'<div class="menu-wrap">';
				$output .= 			'<div class="title">' . $orion_menu_blurb . '</div>';
			}
		} else if ( $depth == 1 ) {
			if ( $this->enabled ) {
				$output .=     '<div class="title">' . $orion_menu_blurb . '</div>';
				$output .=     '<a class="cover" aria-label="'. $orion_menu_blurb .'" href="' . $item->url . '"></a>';
				$this->depth_index += 1;
			}
		}
	}
	public function end_el( &$output, $item, $depth = 0, $args = null ) {
		$orion_mega_enabled = get_field('orion_mega_enabled', $item);
		if ( $orion_mega_enabled ) {
			if ( $depth == 0 ) {
				$this->breakpoint = null;
				$output .= 		'</div>';
				$output .= 	'</div>';
				$output .= '</div>';
			}
		}
		parent::end_el( $output, $item, $depth, $args );
	}
}

