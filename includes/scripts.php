<?php

// kill old styles
function sv_dequeue_unnecessary_styles() {
	$remove = [ 'wp-block-library', 'menu-image', 'if-menu-site-css' ];
	foreach ( $remove as $handle ) {
		wp_dequeue_style( $handle );
		wp_deregister_style( $handle );
	}
}
add_action( 'wp_print_styles', 'sv_dequeue_unnecessary_styles' );

// kill old scripts
function sv_dequeue_unnecessary_scripts() {
	$remove = [ 'wp-embed', 'page-links-to', 'optinmonster-wp-helper' ];
	foreach ( $remove as $handle ) {
		wp_dequeue_script( $handle );
		wp_deregister_script( $handle );
	}
}
add_action( 'wp_print_scripts', 'sv_dequeue_unnecessary_scripts', 11 );

// ENQUEUE STYLES & SCRIPTS
function scripts(){
	
	wp_enqueue_style( 'sv-style', get_template_directory_uri() . '/assets/css/style.min.css', [], filemtime( get_stylesheet_directory() . '/assets/css/style.min.css' ) );
	wp_enqueue_script( 'sv-script-footer', get_template_directory_uri() . '/assets/js/footer.min.js', ['jquery'], filemtime( get_stylesheet_directory() . '/assets/js/footer.min.js' ), true );
	
}
add_action('wp_enqueue_scripts', 'scripts');

// HEAD CLEANUP
function cleanup(){
	remove_action('rest_api_init', 'wp_oembed_register_route');
	remove_action('template_redirect', 'rest_output_link_header', 11, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10);
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head'); 
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'index_rel_link'); 
	remove_action('wp_head', 'parent_post_rel_link', 10);
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_head', 'rel_canonical');
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'start_post_rel_link', 10); 
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wp_oembed_add_discovery_links'); 
	remove_action('wp_head', 'wp_oembed_add_host_js');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_print_styles', 'print_emoji_styles');
}
add_action('after_setup_theme', 'cleanup');
add_filter('emoji_svg_url', '__return_false');
