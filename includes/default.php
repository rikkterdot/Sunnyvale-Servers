<?php

if(!function_exists('setup')){
	function setup(){
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');
		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		]);

		register_nav_menus(['primary' => 'Primary']);
		register_nav_menus(['footer' => 'Footer']);
	}
}
add_action('after_setup_theme', 'setup');

// ALLOW SVGS
function cc_mime_types($mimes){
	$mimes['json'] = 'application/json';
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
function my_correct_filetypes( $data, $file, $filename, $mimes, $real_mime ) {
	if ( ! empty( $data['ext'] ) && ! empty( $data['type'] ) ) {
		return $data;
	}
	$wp_file_type = wp_check_filetype( $filename, $mimes );
	// Check for the file type you want to enable, e.g. 'svg'.
	if ( 'svg' === $wp_file_type['ext'] ) {
		$data['ext']  = 'svg';
		$data['type'] = 'image/svg+xml';
	}
	if ( 'json' === $wp_file_type['ext'] ) {
		$data['ext']  = 'json';
		$data['type'] = 'application/json';
	}
	return $data;
}
add_filter( 'wp_check_filetype_and_ext', 'my_correct_filetypes', 10, 5 );

