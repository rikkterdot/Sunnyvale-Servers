<?php 

add_shortcode( 'title', 'sv_title' );

function sv_title( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'text' => 'Title',
	), $atts );

	$output = '<div class="type-title"><h2>'. esc_attr( $a['text'] ) .'</h2></div>';

	return $output;
}