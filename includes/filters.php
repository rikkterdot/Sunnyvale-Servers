<?php

// CUSTOM EXCERPT
function my_excerpt_length($length){ return 20; }
add_filter('excerpt_length', 'my_excerpt_length');
function new_excerpt_more($excerpt){ return '...'; }
add_filter('excerpt_more', 'new_excerpt_more');

// REMOVE TYPE FROM ARCHIVE TITLE
add_filter('get_the_archive_title', function ($title){
	if(is_category()){
		$title = single_cat_title('', false);
	} elseif(is_tag()){
		$title = single_tag_title('', false);
	} elseif( is_author()){
		$title = '' . get_the_author() . '';
	}
	return $title;
});

// LIMIT SEARCH TO POSTS
function searchfilter($query) {
	if($query->is_search && !is_admin()){
		$query->set('post_type', array('post', 'resource_hub'));
	}
	return $query;
}
add_filter('pre_get_posts','searchfilter');
