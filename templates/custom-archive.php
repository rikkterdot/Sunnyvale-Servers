<?php
/**
 * Template Name: Custom Archive
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();

?>

    <?php get_template_part( 'templates/partials/page', 'title' ); ?>
    <div class="texture-bg">
        <div style="height: 2px;"></div>
        <div class="yellow-bar no-mrg-top"></div>
        <div id="custom_archives" class="">
            <?php 
                if( have_rows('game_list') ):
                    $count = 2;
                    while( have_rows('game_list') ) : the_row(); ?>
                        <div class="game_item wow fadeInDown delay0-<?php echo $count; ?>s">
                            <div class="bg" style="background-image: url(<?php the_sub_field('game_background'); ?>);"></div>
                            <a href="<?php the_sub_field('link'); ?>"></a>
                            <div class="container">
                                <?php
                                    $image = get_sub_field('game_logo');
                                    $fullurl = $image['url'];
                                    $alt = $image['alt'];
                                    $title = $image['title'];
                                ?>
                                <img class="logo" alt="<?php echo $alt; ?>" title="<?php echo $title; ?>" src="<?php echo $fullurl; ?>">
                                <div class="game_list_text">View <?php the_title(); ?></div>
                            </div>
                        </div>
                    <?php $count++; endwhile;
                endif;
            ?>
        </div>
    </div>

<?php get_footer(); ?>
