<?php
/**
 * Template Name: News
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
<link href="<?php bloginfo('template_directory'); ?>/css/blog.css?ver=<?php echo time(); ?>" rel="stylesheet" />

<section id="pageTitle" class="parallax" style="background-image: url(<?php the_field('page_title_image'); ?>);">
	<div class="blackOverlay">
		<h1><?php the_title(); ?></h1>
	</div>
</section>
<div class="greenBar"></div>
<section>
	<div class="container">
	<?php
                // set up or arguments for our custom query
                $paged = ( get_query_var('paged') ) ? absint( get_query_var( 'paged' ) ) : 1;
                $query_args = array(
                    'post_type' => 'post',
                    'posts_per_page' 	=> 6,
                    'paged' => $paged
                );
                // create a new instance of WP_Query
                $the_query = new WP_Query( $query_args );
            ?>
            <div id="blogWrap" class="">
                <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); // run the loop ?>
                    <div class="blog-item">
                        <div class="blogPictureWrap">
                            <a class="imgLink" href="<?php the_permalink(); ?>"></a>
                            <?php if( wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-archive-image' ) ){ ?>
                                <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'blog-archive-image' ); echo $image[0]; ?>" />
                            <?php } else { ?>
                                <?php echo wp_get_attachment_image( '391', 'blog-archive-image'); ?>
                            <?php }; ?>
                        </div>
                        <div class="blogInfoWrap">
                            <h2><?php the_title(); ?></h2>
                            <div class="postDate"><?php the_time('F j, Y'); ?></div>
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="blogLinks">
                            <div class="left">
                                <a class="readMoreLink" href="<?php the_permalink(); ?>">Read More &raquo;</a>
                            </div>
                            <div class="right">
                                <!--<a href="<?php the_permalink(); ?>">Comments <span><?php comments_number( '0', '1', '%' ); ?></span></a>-->
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                    
                <?php endwhile; ?>
            </div>
            <div style="height:30px;"></div>
            <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                <div style="clear:both;"></div>
                <nav class="prev-next-posts">
                    <div>
                        <?php
                        
                            global $the_query; 

                            $big = 999999999; // need an unlikely integer 
                             
                            echo paginate_links( array( 
                                 'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ), 
                                 'format' => '/page/%#%', 
                                 'current' => max( 1, get_query_var('paged') ), 
                                 'total' => $the_query->max_num_pages,
                                 'prev_text'    => __('<div class="prevBtn pnBtn"></div>'),
                                 'next_text'    => __('<div class="nextBtn pnBtn"></div>')
                            ) );
                        
                        ?>
                    </div>
                </nav>
            <?php } ?>
            
            <?php else: ?>
                <article>
                    <h1>Sorry...</h1>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                </article>
            <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>
