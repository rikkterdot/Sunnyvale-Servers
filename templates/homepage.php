<?php
/**
 * Template Name: Homepage
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();

$games = get_terms('game', array('hide_empty' => false, 'parent' => 0));
?>

    <section id="hero_wrap">
        <div class="container">
            <h2 class="wow fadeInDown delay0-2s"><?php the_field('h2_field'); ?></h2>
            <h3 class="wow fadeInDown delay0-4s"><?php the_field('h3_field'); ?></h3>
            <div class="wow fadeInDown delay0-6s">
                <a href="<?php the_field('url'); ?>" class="yellow-angled"<?php if( get_field('open_in_new_window') ): echo "target='_blank'"; endif; ?>><span><?php the_field('button_text'); ?></span></a>
            </div>
        </div>
    </section>
    <section id="main" class="texture-bg">
        <div id="game-servers">
            <div id="game-selection">
                <div id="servers-text">click the game to view servers</div>
                <div class="container">
                    <div id="games-logos-wrap">
                        <?php 
                            $count  = 0;
                            foreach( $games as $game ):
                                $name   = $game->name;
                                $image  = get_field('game_logo', $game);
                        ?>
                            <div class="logo-item<?php if( $count == 0 ): echo " active"; endif; ?>"><img alt="<?php echo $name; ?>" title="<?php echo $name; ?>" src="<?php echo $image['url']; ?>"></div>
                        <?php $count++; endforeach; ?>
                    </div>
                </div>
            </div>
            <div id="main-servers-wrap" class="">
                <?php

                    //loop through the games
                        //loop through server types
                            //loop through games with server type

                    $count = 0;
                    foreach( $games as $game ):
                        $slug = $game->slug;

                ?>

                    <div class="servers<?php if( $count == 0 ): echo " active"; endif; ?>">
                        <div class="container">
                            <?php
                                $servers        = [];
                                $server_types   = [];

                                $the_query = new WP_Query( array(
                                    'post_type' => 'server',
                                    'posts_per_page' => -1,
                                    'tax_query' => array(
                                        array (
                                            'taxonomy'  => 'game',
                                            'field'     => 'slug',
                                            'terms'     => $slug,
                                        )
                                    ),
                                ) );
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $server_type = get_the_terms($post->ID, 'server_type');

                                    if(!in_array($server_type[0]->name, $server_types, true)){
                                        array_push($server_types, $server_type[0]->name);
                                    };
                                    array_push($servers, $post);
                                endwhile;

                                wp_reset_postdata();
                            ?>
                            <?php if( $server_types ): ?>
                                <?php foreach( $server_types as $server_type ): ?>
                                    <div class="server-type">
                                        <div class="type-title">
                                            <h2><?php echo $server_type; ?></h2>
                                        </div>
                                        <div class="servers-wrap">
                                            <?php foreach( $servers as $server ): $id = $server->ID; ?>
                                                <?php 
                                                    $this_type = get_the_terms($id, 'server_type');
                                                    if( $this_type[0]->name == $server_type ):
                                                ?>
                                                    <div class="server-item" style="background-image: url(<?php echo get_the_post_thumbnail_url( $id, 'server-image' ); ?>);">
                                                        <div class="black-grad"></div>
                                                        <div class="white-grad"></div>
                                                        <div class="overlay-hover"></div>
                                                        <div class="server-info-wrap">
                                                            <h3><?php the_field('display_name', $id); ?><span><?php the_field('server_perspective', $id); ?></span></h3>
                                                            <div class="join-with">
                                                                Join With<br>
                                                                <a href="<?php the_field('dzsa_url', $id); ?>" target="_blank">DZSA</a> | <a href="<?php the_field('vanilla_url', $id); ?>" target="_blank">Vanilla</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <h4 class="coming-soon">Coming Soon!</h4>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php $count++; endforeach; ?>
            </div>
        </div>
    </section>
    <section id="custom-gear" class="texture-bg">
        <div class="yellow-bar no-mrg-top"></div>
        <div class="customer-gear-parallax parallax" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/parallax-bg.jpg);">
            <div class="container">
                <div id="custom-stats">
                    <div id="custom-gear-title"></div>
                    <div id="custom-gear-columns">
                        <?php 
                            if( have_rows('column_panels') ):
                                while( have_rows('column_panels') ) : the_row(); ?>
                                    <div class="gear-column wow fadeInDown delay0-2s">
                                        <h3><?php the_sub_field('column_title'); ?></h3>
                                        <ul>
                                            <?php if( have_rows('column_text') ):
                                                while( have_rows('column_text') ) : the_row(); ?>
                                                    <?php 
                                                        $type   = get_sub_field('type');
                                                        $text   = get_sub_field('text');
                                                        $image  = get_sub_field('image');
                                                    ?>
                                                    <li>
                                                        <?php if( $type == 'text' ): ?>
                                                            <?php echo $text; ?>
                                                        <?php else: ?>
                                                            <img src="<?php echo $image['url']; ?>">
                                                        <?php endif; ?>
                                                    </li>
                                                <?php endwhile;
                                            endif; ?>
                                        </ul>
                                        <div class="custom-gear-pricing">
                                            <div class="price">$<?php the_sub_field('usd_price'); ?> <span>USD</span></div>
                                            <div class="price">$<?php the_sub_field('canadian_price'); ?> <span>CAD</span></div>
                                        </div>
                                    </div>
                                <?php endwhile;
                            endif;
                        ?>
                    </div>
                    <a href="#" class="angled-btn wow fadeInDown delay0-8s">View Gallery</a>
                </div>
                <div id="custom-example" class="wow fadeInDown ">
                    <?php $count = 0; foreach( get_field('custom_image_examples') as $custom_example ): ?>
                        <?php 
                            $width      = $custom_example['image']['width'];
                            $height     = $custom_example['image']['height'];
                            $url        = $custom_example['image']['url'];
                            $left       = $custom_example['left_position'];
                            $bottom     = $custom_example['bottom_position'];
                        ?>
                        <div class="custom-example-item <?php echo $count==0 ? 'active' : '';  ?>" style="background-image:url(<?php echo $url; ?>); width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; bottom: <?php echo $bottom; ?>px; left: <?php echo $left; ?>px;"></div>
                    <?php $count++; endforeach; ?>
                </div>
            </div>
        </div>
        <div class="yellow-bar no-mrg-btm"></div>
    </section>
    <section id="custom-merch" class="texture-bg">
        <div class="container">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/custom-merch.png">
        </div>
    </section>

    <script>
        jQuery(document).ready(function($){
            $("#game-servers #game-selection #games-logos-wrap .logo-item").click(function(){
                if( $(this).hasClass("active") ){
                    //do nothing
                } else {
                    var id = $(this).index();

                    //blur list
                    $("#game-servers #main-servers-wrap").addClass("blur").fadeTo(50,1, function(){
                        //change server list
                        $("#game-servers #main-servers-wrap .servers.active").removeClass("active");
                        $("#game-servers #main-servers-wrap .servers").eq(id).addClass("active").fadeTo(50, 1, function(){
                            //remove blur
                            $("#game-servers #main-servers-wrap").removeClass("blur");
                        });
                    });

                    //change logo
                    $("#game-servers #game-selection #games-logos-wrap .logo-item.active").removeClass("active");
                    $(this).addClass("active")
                }
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/slick/slick-theme.css">
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick/slick.js"></script>

    <script>
        jQuery(document).ready(function($){
            $('#game-servers #main-servers-wrap .servers .servers-wrap').slick({
                dots: false,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                variableWidth: true,
                responsive: [
                    {
                      breakpoint: 1500,
                      settings: {
                        slidesToShow: 4
                      }
                    },
                    {
                      breakpoint: 1200,
                      settings: {
                        slidesToShow: 3
                      }
                    },
                ]
            });
            
            var timer = 5000;
            var speed = 2000;
            setTimeout(changeExample, timer);

            function changeExample(){
                var current  = $("#custom-example .custom-example-item.active");
                var currentI = current.index();
                var nextI     = current.next().index();

                if( current.next().length ){
                    //do nothing
                } else {
                    nextI = 0;
                };

                current.fadeOut(speed).removeClass("active");
                $("#custom-example .custom-example-item").eq(nextI).fadeIn(speed).addClass("active");

                setTimeout(changeExample, timer);
            };
        });
    </script>

<?php get_footer(); ?>
