<?php

include( get_template_directory() . '/includes/default.php' );
include( get_template_directory() . '/includes/scripts.php' );
include( get_template_directory() . '/includes/shortcodes.php');
include( get_template_directory() . '/includes/template_functions.php');

add_filter('preview_post_link', function ($link) {
        global $post;
	   return $link . "&page_id=" . $post->ID;
});
