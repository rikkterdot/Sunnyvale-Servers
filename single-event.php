<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

    <?php get_template_part( 'templates/partials/page', 'title' ); ?>
    
    <div class="texture-bg">
        <div style="height: 2px;"></div>
        <div class="yellow-bar no-mrg-top"></div>
        <div id="single-wrap" class="container">
            <div id="labels_wrap">
                <div class="event-label">
                    <h3>When</h3>
                    <p><?php the_field('when'); ?></p>
                </div>
                <div class="event-label">
                    <h3>Where</h3>
                    <p><?php the_field('where'); ?></p>
                </div>
                <div class="event-label">
                    <h3>Prizes</h3>
                    <p><?php the_field('prizes'); ?></p>
                </div>
                <?php if( get_field('winners') ): ?>
                    <div class="event-label">
                        <h3>Winners</h3>
                        <?php the_field('winners'); ?>
                    </div>
                <?php endif; ?>

            </div>
            <div class="yellow-bar smheight lrgmrg"></div>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php 
                    the_content();
                ?>
            <?php endwhile; ?>
        </div>
    </div>

<?php get_footer(); ?>
