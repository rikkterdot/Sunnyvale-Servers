<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>		

            <footer>
                <section id="join-discord">
                    <a href="#">Join Our Discord</a>
                </section>
                <section id="footer-main" class="texture-bg">
                    <div class="container">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>|</li>
                            <li><a href="#">Servers</a></li>
                            <li>|</li>
                            <li><a href="#">Events</a></li>
                            <li>|</li>
                            <li><a href="#">Leaderboards</a></li>
                            <li>|</li>
                            <li><a href="#">Rules</a></li>
                            <li>|</li>
                            <li><a href="#">Discord</a></li>
                            <li>|</li>
                            <li><a href="#">Donate</a></li>
                        </ul>

                        <div id="footer-logo">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sv-logo-custom-gear.png">
                            <div id="copyright">Copyright &copy; 2022 Sunnyvale Servers - All Rights Reserved.</div>
                        </div>
                    </div>
                </section>
            </footer>
        </div>
		<?php wp_footer(); ?>
	</body>
</html>
