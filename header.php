<?php
/**
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<header id="masthead">
			<div class="container">
				<div id="header_inner">
					<div id="logo_wrap">
						<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/sv-logo.png"></a>
					</div>
					<div id="nav_wrap">
						<?php wp_nav_menu( array('menu' => 'Navigation' )); ?>
					</div>
				</div>
			</div>
		</header>
		<div id="event_wrap_main">
			<div class="container">
				<div id="event_wrap">
					<div id="event_wrap_inner">
						<?php 
							// get posts
							$posts = get_posts(array(
								'post_type'         => 'event',
								'posts_per_page'    => 1,
								'meta_key'          => 'date_picker',
								'orderby'           => 'meta_value',
								'order'             => 'DESC'
							));
						?>
						<div id="event_wrap_title"><span>Event Alert</span></div>
						<?php if( $posts ): ?>
							<?php foreach( $posts as $post ): setup_postdata( $post ); ?>
								<div id="event_info">
									<?php the_field('when'); ?> - <?php the_title(); ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="blue_btn">Learn More</a>
							<?php endforeach; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="site_wrap">